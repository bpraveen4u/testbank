﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using TestBank.ExamPoint.Models;
using TestBank.ExamPoint.Web.Infrastructure.ServiceProxy;
using TestBank.Entity.Models;
using TestBank.ExamPoint.Web.Infrastructure.Security;
using System.Threading;
using System.Security.Principal;
using TestBank.Entity.Model;
using System.Web.Security;
using Newtonsoft.Json;

namespace TestBank.ExamPoint.Web.Controllers
{
    public class AccountController : BaseController
    {
        string onlineUrl = ConfigurationManager.AppSettings.Get("onlineUrl");
        string domain = ConfigurationManager.AppSettings.Get("topLevelDomain");

        public ActionResult Index()
        {
            return View();
        }

        public RedirectResult Login()
        {
            
            return Redirect(string.Format("{0}Account/Login", onlineUrl));
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public void Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ApiKey = ApiServiceProxy.Authenticate(new Credentials() { User = model.UserName, Password = model.Password });
                    if (ApiKey != null && ApiKey != "")
                    {
                        //string userData = "ApplicationSpecific data for this user.";
                        var identity = new CustomTestBankIdentity(new User() { Id = model.UserName });
                        Thread.CurrentPrincipal = new GenericPrincipal(identity, new string[] { "" });
                        
                        FormsAuthentication.SetAuthCookie(model.UserName, false);
                        //FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                        //                model.UserName,
                        //                DateTime.Now,
                        //                DateTime.Now.AddMinutes(30),
                        //                true,
                        //                "",
                        //                FormsAuthentication.FormsCookiePath);

                        // Encrypt the ticket.
                        //string encTicket = FormsAuthentication.Encrypt(ticket);

                        // Create the cookie.
                        //HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                        //cookie.Domain = domain;
                        //cookie.HttpOnly = true;
                        //Response.Cookies.Add(cookie);

                        HttpCookie cookie = FormsAuthentication.GetAuthCookie(User.Identity.Name.ToString(), false);
                        cookie.Domain = domain;//the second level domain name
                        Response.AppendCookie(cookie);

                        Response.Write(JsonConvert.SerializeObject( new { status = "success", message = model.UserName }));
                        return;
                        //return RedirectToLocal(returnUrl);
                    }
                }
                catch (ApiError e)
                {
                    if (e.Errors != null && e.Errors.Count() > 0)
                    {
                        Response.Write(JsonConvert.SerializeObject(new { status = "failed", message = e.Errors.ToList()[0] }));
                        return;
                    }
                    else
                    {
                        Response.Write(JsonConvert.SerializeObject(new { status = "failed", message = e.ToString() }));
                        return;
                    }
                    
                    
                    //ModelState.AddModelError("", e.Errors.First());
                    //return View(model);
                }

            }

            // If we got this far, something failed, redisplay form
            Response.Write(JsonConvert.SerializeObject(new { status = "failed", message = "The user name or password provided is incorrect." }));
            //ModelState.AddModelError("", "The user name or password provided is incorrect.");
            //return View(model);
        }

        public RedirectResult Register()
        {
            return Redirect(string.Format("{0}Home/index?apikey={1}", onlineUrl, ApiKey));
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}
