﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;
using System.Configuration;
using RestSharp.Serializers;
using RestSharp.Deserializers;
using Newtonsoft.Json;
using System.IO;
using System.Threading;
using System.Net;
using TestBank.Entity.Models;

namespace TestBank.ExamPoint.Web.Infrastructure.ServiceProxy
{
    public static class ApiServiceProxy
    {
        public static string BaseAddress = ConfigurationManager.AppSettings.Get("ApiBaseUrl");
        public static string Authenticate(Credentials credentials)
        {
            //RestClient client = new RestClient(BaseAddress);

            var request = new RestRequest();
            request.Method = Method.POST;
            request.Resource = "api/Auth";
            request.RequestFormat = DataFormat.Json;
            request.AddBody(credentials);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Accept-Language", "en-US");

            var response = ExecuteHttpRequest(request);
            return response;
        }

        private static RestClient GetHttpClient()
        {
            var client = new RestClient(BaseAddress)
            {
                Authenticator = new HttpBasicAuthenticator("Praveen", "Password")
            };

            client.RemoveHandler("application/json");
            client.AddHandler("application/json", new JsonDotNetSerializer());

            return client;
        }

        public static T ExecuteHttpRequest<T>(RestRequest request) where T : new()
        {
            RestClient client = GetHttpClient();
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");

            var response = client.Execute<T>(request);
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created || response.StatusCode == HttpStatusCode.NoContent)
            {
                return response.Data;
            }
            else
            {
                if (response.StatusCode == System.Net.HttpStatusCode.PreconditionFailed || response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    var enumConverter = new Newtonsoft.Json.Converters.StringEnumConverter();
                    var errors = Newtonsoft.Json.JsonConvert.DeserializeObject<IEnumerable<String>>(response.Content, enumConverter);
                    throw new ApiError(response.StatusDescription, errors.ToList());
                }
                else if (response.StatusCode == 0) // while the input params passed as null
                {
                    throw new ApiError(response.ErrorMessage);
                }
                else if (string.IsNullOrEmpty(response.Content))
                {
                    throw new ApiError(response.StatusDescription);
                }
                else
                {
                    throw new ApiError(response.StatusDescription, new List<string>() { response.Content });
                }
            }
        }

        public static string ExecuteHttpRequest(RestRequest request)
        {
            RestClient client = GetHttpClient();
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");

            var response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created || response.StatusCode == HttpStatusCode.NoContent)
            {
                return response.Content;
            }
            else
            {
                if (response.StatusCode == System.Net.HttpStatusCode.PreconditionFailed || response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    var enumConverter = new Newtonsoft.Json.Converters.StringEnumConverter();
                    var errors = Newtonsoft.Json.JsonConvert.DeserializeObject<IEnumerable<String>>(response.Content, enumConverter);
                    throw new ApiError(response.StatusDescription, errors.ToList());
                }
                else if (response.StatusCode == 0) // while the input params passed as null
                {
                    throw new ApiError(response.ErrorMessage);
                }
                else if (string.IsNullOrEmpty(response.Content))
                {
                    throw new ApiError(response.StatusDescription);
                }
                else
                {
                    throw new ApiError(response.StatusDescription, new List<string>() { response.Content });
                }
            }
        }
    }

    public class JsonDotNetSerializer : ISerializer, IDeserializer
    {
        private readonly Newtonsoft.Json.JsonSerializer _serializer;

        /// <summary>
        /// Default serializer
        /// </summary>
        public JsonDotNetSerializer()
        {
            ContentType = "application/json";

            _serializer = new Newtonsoft.Json.JsonSerializer
            {
                MissingMemberHandling = MissingMemberHandling.Ignore,
                NullValueHandling = NullValueHandling.Include,
                DefaultValueHandling = DefaultValueHandling.Include,
                DateTimeZoneHandling = DateTimeZoneHandling.Utc
            };
        }

        /// <summary>
        /// Default serializer with overload for allowing custom Json.NET settings
        /// </summary>
        public JsonDotNetSerializer(Newtonsoft.Json.JsonSerializer serializer)
        {
            ContentType = "application/json";
            _serializer = serializer;
        }

        /// <summary>
        /// Serialize the object as JSON
        /// </summary>
        /// <param name="obj">Object to serialize</param>
        /// <returns>JSON as String</returns>
        public string Serialize(object obj)
        {
            using (var stringWriter = new StringWriter())
            {
                using (var jsonTextWriter = new JsonTextWriter(stringWriter))
                {
                    jsonTextWriter.Formatting = Formatting.Indented;
                    jsonTextWriter.QuoteChar = '"';

                    _serializer.Serialize(jsonTextWriter, obj);

                    var result = stringWriter.ToString();
                    return result;
                }
            }
        }

        public T Deserialize<T>(IRestResponse response)
        {
            try
            {
                var deserializedObject = JsonConvert.DeserializeObject<T>(response.Content, new JsonSerializerSettings()
                {
                    DateTimeZoneHandling = DateTimeZoneHandling.Local,
                    Culture = Thread.CurrentThread.CurrentCulture
                });

                return deserializedObject;
            }
            catch (Exception)
            {
                return default(T);
            }

        }

        /// <summary>
        /// Unused for JSON Serialization
        /// </summary>
        public string DateFormat { get; set; }

        /// <summary>
        /// Unused for JSON Serialization
        /// </summary>
        public string RootElement { get; set; }
        /// <summary>
        /// Unused for JSON Serialization
        /// </summary>
        public string Namespace { get; set; }
        /// <summary>
        /// Content type for serialized content
        /// </summary>
        public string ContentType { get; set; }
    }
}