﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using TestBank.Entity.Model;

namespace TestBank.ExamPoint.Web.Infrastructure.Security
{
    public class CustomTestBankIdentity : IIdentity
    {
        public User User { get; private set; }

        public CustomTestBankIdentity(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");
 
            this.User = user;
        }

        public string AuthenticationType
        {
            get { return "CustomeTestBankExamPoint"; }
        }

        public bool IsAuthenticated
        {
            get { return true; }
        }

        public string Name
        {
            get { return User.Id; }
        }
    }
}