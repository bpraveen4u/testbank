﻿
$(document).ready(function () {
    /*if ($('#body').height() < $(document).height())
    $('#body').height($(document).height() - 211);*/
    //alert($('#header').height() + ":" + $('#footer').height());

    $('.loginStatus').hide();
    $('.welcomeUser').hide();
    $('#liMyAccount').hide();
    $('#menu li').click(function () {
        $('#menu li').removeClass('current_page_item');
        $(this).addClass('current_page_item');
    });


    $('.btnLogin').bind('click', function () {
        if (validate()) {
            login();
        }
    });
});

function validate() {

    if ($('#txtUserName').val() == "") {
        alert("Please enter user name.");
        $('#txtUserName').focus();
        return false;
    }

    if ($('#txtPassword').val() == "") {
        alert("Please enter password.");
        $('#txtPassword').focus();
        return false;
    }
    return true;
}

function login() {
    var postData = { "UserName": $('#txtUserName').val(), "Password": $('#txtPassword').val() };

    $.ajax({
        cache: false,
        async: false,
        type: "POST",
        url: "Account/Login",
        data: postData,
        dataType: "json",
        //contentType: "application/json",
        success: function (objData) {
            if (objData && objData.status == "success") {
                $('.welcomeUser').html("Welcome <b>" + objData.message + "!</b>");
                $('.loginFields').hide();
                $('.loginStatus').hide();
                $('.welcomeUser').show();
                $('#liMyAccount').show();
            }
            else {
                $('.loginStatus').html(objData.message);
                $('.loginStatus').show();
                $('#txtUserName').val('');
                $('#txtPassword').val('')
                $('#txtUserName').focus();
            }
        },
        error: function (xhr) {
            alert('error');
        }
    });
}
