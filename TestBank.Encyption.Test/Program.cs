﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestBank.QueryStringEncryption.QueryString;
using TestBank.QueryStringEncryption.Utility;
using System.Security.Cryptography;
using System.IO;

namespace TestBank.Encyption.Test
{
    public class Program
    {
        static void Main(string[] args)
        {
            //SecureQueryString qs = new SecureQueryString("123345678901234567890123456789");
   
            //qs["YourName"] = "testbank1234567890123456789001234567890qqqqqqqqqqqqqqqqqqqqqqqwmnbvcxz";
            //qs.ExpireTime = DateTime.Now.AddMinutes(2);


            //try
            //{
            //    var enc = qs.ToString();
            //    Console.WriteLine(enc);
            //    SecureQueryString qs1 = new SecureQueryString("123345678901234567890123456789", enc);

            //    Console.Write("Your name is {0}", qs1["YourName"]);
            //}
            //catch (Exception ex)
            //{

            //}

            var str = "q=PrependIVToCipher&aq=f&oq=PrependIVToCipher&aqs=chrome.0.57.2402j0&sourceid=chrome&ie=UTF-8";
            //var encdec = new EncrtptDecryptQ();
            var enc = EncryptDecryptQueryString.Encrypt(str);
            Console.WriteLine(enc);

            Console.WriteLine();

            Console.WriteLine(EncryptDecryptQueryString.Decrypt(enc));

            Console.ReadLine();
        }
    }

    public class EncrtptDecrypt
    {
        private static string EncryptionKey = "!#s@3g`d";
        private static byte[] key = { };
        private static byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };

        public string Encrypt(string Input)
        {
            try
            {
                key = System.Text.Encoding.UTF8.GetBytes(EncryptionKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                Byte[] inputByteArray = Encoding.UTF8.GetBytes(Input);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string Decrypt(string Input)
        {
            Byte[] inputByteArray = new Byte[Input.Length];

            try
            {
                key = System.Text.Encoding.UTF8.GetBytes(EncryptionKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(Input);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                Encoding encoding = Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            catch (Exception ex)
            {
                return "";
            }
        }
 
    }
}
