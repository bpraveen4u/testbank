﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Client.Document;
using Raven.Client;
using System.Linq.Expressions;
using TestBank.Entity.Utilities;

namespace TestBank.Repository
{
    public class TestBankRepository : IRepository
    {
        private DocumentStore _store;
        private IDocumentSession _session;

        public IDocumentSession RavenSession
        {
            get
            {
                return _session;
            }
        }

        public DocumentStore Store
        {
            get
            {
                return _store;
            }
        }

        public TestBankRepository()
        {
            var documentStore = new Raven.Client.Document.DocumentStore { ConnectionStringName = "TestBankConn" };
            documentStore.Initialize();
            //var generator = new MultiTypeHiLoKeyGenerator(documentStore, 10);
            //documentStore.Conventions.DocumentKeyGenerator =
            //    entity => generator.GenerateDocumentKey(documentStore.Conventions, entity);
            documentStore.Conventions.DocumentKeyGenerator = (dbname, command, entity) => documentStore.Conventions.GetTypeTagName(entity.GetType()) + "/";
            _store = documentStore;
            _session = _store.OpenSession();
        }

        public TestBankRepository(DocumentStore store)
        {
            _store = store;
            _session = _store.OpenSession();
        }

        public T SingleOrDefault<T>(Func<T, bool> predicate) where T : Entity.IModel
        {
            return _session.Query<T>().SingleOrDefault(predicate);
        }

        public T Load<T>(string id) where T : Entity.IEntity
        {
            //for performance reasons
            return _session.Load<T>(id);
        }

        public ILoaderWithInclude<T> Include<T>(Expression<Func<T, object>> path) where T : Entity.IModel
        {
            return _session.Include<T>(path);
        }

        public T[] IncludeWithLoad<T>(Expression<Func<T, object>> path, params string[] ids) where T : Entity.IModel
        {
            return _session.Include<T>(path).Load(ids);
            //Raven.Client.Linq.RavenQueryStatistics stats;
            //_session.Query<T>().Customize(x => x.Include<
            //
        }

        

        public IEnumerable<T> All<T>() where T : Entity.IModel
        {
            return _session.Query<T>().OrderBy(m => m.Sort);
        }
        
        public IEnumerable<T> All<T>(int pageIndex, int pageSize, Expression<Func<T,object>> sortBy, out int totalResults) where T : Entity.IModel
        {
            //return _session.Query<T>().OrderBy(x => x.Id).Skip((pageIndex - 1) * pageSize).Take(pageSize);
            
            Raven.Client.RavenQueryStatistics stats;
            //var session = _repository.RavenSession;
            var pagedItems = _session.Query<T>()
                .Statistics(out stats)
                .OrderBy(sortBy)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToList()
                ;
            totalResults = stats.TotalResults;

            return pagedItems;
            //var documentQuery = _session.Advanced.LuceneQuery<T>()
            //    .OrderBy("Id")
            //    .Skip((pageIndex - 1) * pageSize)
            //    .Take(pageSize);

            //return documentQuery.ToList();
        }

        public IEnumerable<T> All<T>(int pageIndex, int pageSize, Expression<Func<T, object>> filterBy, object value) where T : Entity.IModel
        {
            if (pageIndex < 1)
            {
                throw new Exception("pageIndex must be greater than 0");
            }

            if (pageSize < 1)
            {
                throw new Exception("pageSize must be greater than 0");
            }

            var documentQuery = _session.Advanced.LuceneQuery<T>()
                .WhereEquals(filterBy, value)
                .OrderBy("Id")
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize);
            return documentQuery.ToList();
        }

        public int Count<T>() where T : Entity.IModel
        {
            var documentQuery = _session.Advanced.LuceneQuery<T>()
                .Take(3);

            var pagedCustomers = documentQuery.ToList();
            var totalResults = documentQuery.QueryResult.TotalResults;
            return totalResults;

        }

        public int Count<T>(Expression<Func<T, object>> propertySelector, object value) where T : Entity.IModel
        {
            //return _session.Query<Raven.Abstractions.Data>();
            
            var documentQuery = _session.Advanced.LuceneQuery<T>()
                .WhereEquals(propertySelector, value)
                .Take(3);

            //var pagedCustomers = documentQuery.ToList();
            var totalResults = documentQuery.QueryResult.TotalResults;
            return totalResults;

        }

        public void Delete<T>(T item) where T : Entity.IModel
        {
            _session.Delete(item);
        }

        public void Add<T>(T item) where T : Entity.IEntity
        {
            _session.Store(item);
        }

        public void Save()
        {
            _session.SaveChanges();
        }
    }
}
