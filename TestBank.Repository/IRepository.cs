﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestBank.Entity;
using Raven.Client.Document;
using System.Linq.Expressions;
using Raven.Client;

namespace TestBank.Repository
{
    public interface IRepository
    {
        T SingleOrDefault<T>(Func<T, bool> predicate) where T : IModel;
        T Load<T>(string id) where T : Entity.IEntity;
        ILoaderWithInclude<T> Include<T>(Expression<Func<T, object>> path) where T : Entity.IModel;
        T[] IncludeWithLoad<T>(Expression<Func<T, object>> path, params string[] ids) where T : Entity.IModel;
        IEnumerable<T> All<T>() where T : IModel;
        IEnumerable<T> All<T>(int pageIndex, int pageSize, Expression<Func<T, object>> sortBy, out int totalResults) where T : IModel;
        IEnumerable<T> All<T>(int pageIndex, int pageSize, Expression<Func<T, object>> filterBy, object value) where T : Entity.IModel;
        int Count<T>() where T : Entity.IModel;
        int Count<T>(Expression<Func<T, object>> propertySelector, object value) where T : Entity.IModel;
        void Delete<T>(T item) where T : IModel;
        void Add<T>(T item) where T : IEntity;
        void Save();
        IDocumentSession RavenSession {get;}
        DocumentStore Store { get; }
    }
}
