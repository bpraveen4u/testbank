﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace TestBank.Entity.Utilities
{
    public class RavenIdResolver
    {
        private static string Resolve(string ravenId)
        {
            if (string.IsNullOrWhiteSpace(ravenId))
            {
                return null;
            }
            var match = Regex.Match(ravenId, @"\d+");
            var idStr = match.Value;
            string id = int.Parse(idStr).ToString();
            //if (id == 0)
            //    throw new System.InvalidOperationException("Id cannot be zero."); // TODO: use code contracts.
            return id;
        }
    }
}
