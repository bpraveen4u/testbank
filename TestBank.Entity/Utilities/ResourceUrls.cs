﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestBank.Entity.Utilities
{
    public static class ResourceUrls
    {
        public static string Question_All = "api/Questions";
        //public static string Question_All_Count = "api/Questions?countOnly={countOnly}";
        public static string Question_Get = "api/Questions/{id}";
        public static string Question_Post = "api/Questions";
        public static string Question_Put = "api/Questions/{id}";

        public static string Assessments_All = "api/Assessments";
        public static string Assessments_Get = "api/Assessments/{id}";
        public static string Assessments_Post = "api/Assessments";
        public static string Assessments_Put = "api/Assessments/{id}";

        public static string UserAnswers_All = "api/UserAnswers?testId={testId}";
        public static string UserAnswers_Post = "api/UserAnswers";
        public static string UserAnswers_Put = "api/UserAnswers/{id}";
        public static string UserAnswers_Get = "api/UserAnswers/{id}";

        public static string User_Get = "api/Users/{id}";
        //public static string User_Get_UserId = "api/Users?userId={userId}";
        public static string User_Post = "api/Users";
    }
}
