﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestBank.Entity.Utilities
{
    public enum RavenIdPrefix
    {
        Questions,
        Assessments,
        Users,
        UserAnswers,
        ApiKeyStores
    }

    public class RavenIdConverter
    {
        public static string Convert(RavenIdPrefix model, int id)
        {
            return string.Format("{0}/{1}", model.ToString(), id);
        }

        public static string Convert(RavenIdPrefix model, string id)
        {
            return string.Format("{0}/{1}", model.ToString(), id);
        }
    }
}
