﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestBank.Entity.Model
{
    public class PagedModel<T> where T: IModel
    {
        public int TotalRecords { get; set; }
        public int PageSize { get; set; }
        public int CurrenctPage { get; set; }

        public List<T> PagedData { get; set; }

        public PagedModel()
        {

        }
    }
}
