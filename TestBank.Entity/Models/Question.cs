﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestBank.Entity.Model
{
    public class Question : IModel
    {
        public string Id {get; set; }
        public int Sort { get; set; }
        public string Description { get; set; }
        public string InstructorRemarks { get; set; }
        public string Category { get; set; }
        public List<Option> Options { get; set; }
        public byte Weightage { get; set; }
        public float CorrectScore { get; set; }
        public float WrongScore { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedUser { get; set; }

        public Question()
        {
        }
    }

    public class QuestionReference
    {
        public string QuestionId { get; set; }
        public int Sort { get; set; }
    }
}
