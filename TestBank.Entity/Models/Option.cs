﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.ComponentModel;

namespace TestBank.Entity.Model
{
    [DefaultValue(RadioButton)]
    public enum OptionType
    {
        None = 0,
        RadioButton = 1,
        CheckBox = 2,
        DropDown = 3,
        Text = 4
    }
    
    public class Option
    {
        public string Id { get; set; }
        public int Sort { get; set; }
        public OptionType Type { get; set; }
        public string Text { get; set; }
        public bool IsCorrect { get; set; }
        //public List<Option> Options { get; set; }
        [JsonIgnore]
        public bool IsSerializeAnswer { get; private set; }

        public Option()
        {
            IsSerializeAnswer = true;
            IsCorrect = false;
            Type = OptionType.RadioButton;
        }

        public Option(bool isSerializeAnswer)
        {
            IsSerializeAnswer = isSerializeAnswer;
            IsCorrect = false;
        }

        public void SetAnswerSerialize(bool value)
        {
            IsSerializeAnswer = value;
        }

        /// <summary>
        /// 
        /// http://james.newtonking.com/projects/json/help/index.html?topic=html/ConditionalProperties.htm
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeIsCorrect()
        {
            return IsSerializeAnswer;
        }
    }
}
