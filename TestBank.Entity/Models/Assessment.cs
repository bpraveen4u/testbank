﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace TestBank.Entity.Model
{
    public enum TestStatus
    {
        None = 0,
        Started = 1,
        Completed = 2,
    }
    public class Assessment : IModel
    {
        public string Id { get; set; }
        public int Sort { get; set; }
        public string TestName { get; set; }
        public List<QuestionReference> Questions { get; set; }
        public List<Question> QuestionDetails { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedUser { get; set; }
        public string Link { get; set; }
        public string ShortLink { get; set; }
        public TestStatus Status { get; set; }
        public bool Enable { get; set; }
        public int Duration { get; set; }
        public int MaxOptions { get; set; }
        [JsonIgnore]
        public bool IsSerializeQuestionDetails { get; set; }
        
        public int TotalQuestions
        {
            get
            {
                if (Questions != null)
                {
                    return Questions.Count;
                }
                return 0;
            }
        }

        public Assessment()
        {
            IsSerializeQuestionDetails = true;
        }

        public bool ShouldSerializeQuestionDetails()
        {
            return IsSerializeQuestionDetails;
        }

        public bool ShouldSerializeTotalQuestions()
        {
            return IsSerializeQuestionDetails;
        }
    }

    class AssessmentReference
    {
        public string AssessmentId { get; set; }
        
    }
}
