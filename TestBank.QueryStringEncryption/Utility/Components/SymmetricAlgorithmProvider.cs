﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestBank.QueryStringEncryption.Utility.Components.Interfaces;
using System.Security.Cryptography;
using System.IO;

namespace TestBank.QueryStringEncryption.Utility.Components
{
    public class SymmetricAlgorithmProvider : ISymmetricCryptoProvider
    {
        int IVSize;
        SymmetricAlgorithm algorithm;

        public SymmetricAlgorithmProvider(byte[] key)
            : this(RijndaelManaged.Create(), key)
        {
            
        }

        public SymmetricAlgorithmProvider(SymmetricAlgorithm algorithm, byte[] key)
        {
            this.algorithm = algorithm;
            algorithm.Key = key;
            algorithm.GenerateIV();
            IVSize = algorithm.IV.Length;
        }

        public byte[] Encrypt(byte[] plainText)
        {
            ValidateByteArrayParam("plaintext", plainText);

            algorithm.GenerateIV();

            byte[] ciphertext = null;

            ICryptoTransform transform__1 = algorithm.CreateEncryptor() as ICryptoTransform;
            if (transform__1 != null)
            {
                ciphertext = Transform(transform__1, plainText);
            }
            
            return PrependIVToCipher(ciphertext);
        }

        public byte[] Decrypt(byte[] ciphertext)
        {
            ValidateByteArrayParam("ciphertext", ciphertext);

            algorithm.IV = GetIVFromCipher(ciphertext);

            byte[] plaintext = null;
            ICryptoTransform transform__1 = algorithm.CreateDecryptor() as ICryptoTransform;
            if (transform__1 != null)
            {
                plaintext = Transform(transform__1, StripIVFromCipher(ciphertext));
            }
            
            return plaintext;
        }

        private byte[] Transform(ICryptoTransform transformation, byte[] buffer)
        {
            byte[] returnBuffer = null;

            using (var stream = new MemoryStream())
            {
                CryptoStream cryptoStream = null;

                try
                {
                    cryptoStream = new CryptoStream(stream, transformation, CryptoStreamMode.Write);
                    cryptoStream.Write(buffer, 0, buffer.Length);
                    cryptoStream.FlushFinalBlock();
                    returnBuffer = stream.ToArray();
                }
                catch (Exception)
                {
                }
                finally
                {
                    if (cryptoStream != null)
                    {
                        cryptoStream.Close();
                    }
                }
            }

            return returnBuffer;
        }

        private void ValidateByteArrayParam(string paramName, byte[] value)
        {
            if (paramName == null || value.Length == 0)
            {
                throw new ArgumentNullException(paramName);
            }
        }

        private byte[] PrependIVToCipher(byte[] ciphertext)
        {
            byte[] buffer__1 = new Byte[ciphertext.Length + (algorithm.IV.Length)];
            Buffer.BlockCopy(algorithm.IV, 0, buffer__1, 0, algorithm.IV.Length);
            Buffer.BlockCopy(ciphertext, 0, buffer__1, algorithm.IV.Length, ciphertext.Length);

            return buffer__1;
        }

        private byte[] GetIVFromCipher(byte[] ciphertext)
        {
            byte[] buffer__1 = new Byte[IVSize];
            Buffer.BlockCopy(ciphertext, 0, buffer__1, 0, IVSize);
            return buffer__1;
        }

        private byte[] StripIVFromCipher(byte[] ciphertext){
            byte[] buffer__1 = new Byte[ciphertext.Length - IVSize];
            Buffer.BlockCopy(ciphertext, IVSize, buffer__1, 0, buffer__1.Length);
            return buffer__1;
        }
    }
}
