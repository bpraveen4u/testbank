﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestBank.QueryStringEncryption.Utility.Components.Interfaces
{
    public interface ISymmetricCryptoProvider
    {
        byte[] Encrypt(byte[] plainText);
        byte[] Decrypt(byte[] ciphertext);
    }
}
