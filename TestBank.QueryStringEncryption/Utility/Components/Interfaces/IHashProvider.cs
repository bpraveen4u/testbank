﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestBank.QueryStringEncryption.Utility.Components.Interfaces
{
    public interface IHashProvider
    {
        /// <summary>
        /// Computes the hash value from bytes.
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        byte[] Hash(byte[] buffer);
    }
}
