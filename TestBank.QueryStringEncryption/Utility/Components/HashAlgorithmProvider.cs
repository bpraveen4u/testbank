﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestBank.QueryStringEncryption.Utility.Components.Interfaces;
using System.Security.Cryptography;

namespace TestBank.QueryStringEncryption.Utility.Components
{
    public class HashAlgorithmProvider : IHashProvider
    {
        HashAlgorithm algorithm;

        public HashAlgorithmProvider()
            : this(MD5CryptoServiceProvider.Create())
        {

        }

        public HashAlgorithmProvider(HashAlgorithm algorithm)
        {
            this.algorithm = algorithm;
        }

        public byte[] Hash(byte[] buffer)
        {
            return algorithm.ComputeHash(buffer);
        }
    }
}
