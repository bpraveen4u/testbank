﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestBank.QueryStringEncryption.Utility.Interfaces
{
    public interface IEncryptionUtility
    {
        string Encrypt(string input);
        string Decrypt(string input);
        byte[] CombineBytes(byte[] buffer1, byte[] buffer2);
    }
}
