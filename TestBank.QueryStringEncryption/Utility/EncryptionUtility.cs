﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestBank.QueryStringEncryption.Utility.Interfaces;
using TestBank.QueryStringEncryption.Utility.Components.Interfaces;
using TestBank.QueryStringEncryption.Utility.Components;

namespace TestBank.QueryStringEncryption.Utility
{
    public class EncryptionUtility : IEncryptionUtility
    {
        byte[] key;
        ISymmetricCryptoProvider m_symmetricCryptoProvider;
        IHashProvider m_hashProvider;

        public EncryptionUtility(string key)
        {
            this.key = System.Text.Encoding.UTF8.GetBytes(key.Substring(0, 16));
            m_symmetricCryptoProvider = new SymmetricAlgorithmProvider(this.key);
            m_hashProvider = new HashAlgorithmProvider();
        }

        public string Encrypt(string input)
        {
            Byte[] hashLength = null;
            byte[] signedBytes = null;

            try
            {
                var cipherText = m_symmetricCryptoProvider.Encrypt(Encoding.Unicode.GetBytes(input));
                var hash = m_hashProvider.Hash(CombineBytes(key, cipherText));
                signedBytes = CombineBytes(hash, cipherText);
                hashLength = new byte[] { Convert.ToByte(hash.Length) };
            }
            catch (Exception ex)
            {
                throw new ApplicationException("encrypt method failed", ex);
            }

            return Convert.ToBase64String(CombineBytes(hashLength, signedBytes));
        }

        public string Decrypt(string input)
        {
            byte[] plainText = null;

            try
            {
                byte[] inputBytes = Convert.FromBase64String(input);

                var hashLength = inputBytes[0];
                var hash = new Byte[hashLength];
                Buffer.BlockCopy(inputBytes, 1, hash, 0, hashLength);
                var cipherText = new Byte[inputBytes.Length - hashLength - 1];
                Buffer.BlockCopy(inputBytes, hashLength+1, cipherText, 0, cipherText.Length);
                var compareHash = m_hashProvider.Hash(CombineBytes(key, cipherText));

                if(!CommonUtil.CompareBytes(hash, compareHash))
                    throw new ApplicationException("input value was improperly signed or tampered with");

                plainText = m_symmetricCryptoProvider.Decrypt(cipherText);
            }
            catch (Exception generatedExceptionName)
            {
                throw new ApplicationException("decrypt method failed");
            }

            return Encoding.Unicode.GetString(plainText);
        }

        public byte[] CombineBytes(byte[] buffer1, byte[] buffer2)
        {
            byte[] combinedBytes = null;

            try
            {
                combinedBytes = new Byte[buffer1.Length + (buffer2.Length)];
                Buffer.BlockCopy(buffer1, 0, combinedBytes, 0, buffer1.Length);
                Buffer.BlockCopy(buffer2, 0, combinedBytes, buffer1.Length, buffer2.Length);
            }
            catch (Exception generatedExceptionName)
            {
                throw new ApplicationException("combine method failed");
            }

            return combinedBytes;
        }
    }
}
