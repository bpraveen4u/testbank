﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using TestBank.QueryStringEncryption.Utility.Interfaces;
using TestBank.QueryStringEncryption.Utility;

namespace TestBank.QueryStringEncryption.QueryString
{
    public class SecureQueryString : NameValueCollection
    {
        private string timeStampKey = "__TS__";
        private string dateFormat = "G";
        private IEncryptionUtility mEncryptionUtil;
        private DateTime m_expireTime = DateTime.MaxValue;

        public SecureQueryString(string key)
            : base()
        {
            mEncryptionUtil = new EncryptionUtility(key);
        }

        public SecureQueryString(string key, string queryString)
            : this(key)
        {
            Deserialize(DecryptAndVerify(queryString));
            CheckExpiration();
        }

        public override string ToString()
        {
            return EncryptAndSign(Serialize());
        }

        private void Deserialize(string queryString)
        {
            string[] nameValuePairs = queryString.Split('&');
            for(int i = 0; i< nameValuePairs.Length; i++)
            {
                string[] nameValue = nameValuePairs[i].Split('=');
                if( nameValue.Length == 2 )
                    base.Add(nameValue[0], nameValue[1]);
            }

            if (base.GetValues(timeStampKey) != null)
            {
                string[] strExpireTime = base.GetValues(timeStampKey);
                m_expireTime = Convert.ToDateTime(strExpireTime[0]);
            }
        }

        private string Serialize()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var key in base.AllKeys)
	        {
		        sb.Append(key);
                sb.Append('=');
                sb.Append(base.GetValues(key)[0].ToString());
                sb.Append('&');
	        }

            sb.Append(timeStampKey);
            sb.Append('=');
            sb.Append(m_expireTime.ToString(dateFormat));

            return sb.ToString();
        }

        private string DecryptAndVerify(string input)
        {
            return mEncryptionUtil.Decrypt(input);
        }

        private string EncryptAndSign(string input)
        {
            return mEncryptionUtil.Encrypt(input);
        }

        private void CheckExpiration()
        {
            if(DateTime.Compare(m_expireTime, DateTime.Now) < 0 )
                throw new ExpiredQueryStringException();
            
        }

        public DateTime ExpireTime {
            get
            {
                return m_expireTime;
            }
            set
            {
                m_expireTime = value;
            }
        }
       
    }
}
