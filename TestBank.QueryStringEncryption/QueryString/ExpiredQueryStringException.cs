﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestBank.QueryStringEncryption.QueryString
{
    public class ExpiredQueryStringException : Exception
    {
        public ExpiredQueryStringException()
            : base()
        {

        }
    }
}
