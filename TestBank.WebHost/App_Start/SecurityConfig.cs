﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using TestBank.Business.Auth;

namespace TestBank.WebHost
{
    public class SecurityConfig
    {
        public static void ConfigureGlobal(HttpConfiguration globalConfig)
        {
            globalConfig.MessageHandlers.Add(new AuthenticationHandler());
            //globalConfig.Filters.Add(new SecurityExceptionFilter());
        }
    }
}