﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Reflection;

namespace TestBank.WebHost
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var route = config.Routes.MapHttpRoute(
                name: "APIDefault",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }

    //public class CustomAssemblyResolver : IAssembliesResolver
    //{
    //    public ICollection<Assembly> GetAssemblies()
    //    {
    //        List<Assembly> baseAssemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();
    //        var controllersAssembly = Assembly.LoadFrom(AppDomain.CurrentDomain.BaseDirectory + @"bin\TestBank.Business.dll");
    //        baseAssemblies.Add(controllersAssembly);
    //        return baseAssemblies;
    //    }
    //}
}
