﻿using System.Web;
using System.Web.Mvc;
using TestBank.Business.Filters;
using System.Web.Http.Filters;

namespace TestBank.WebHost
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterHttpFilters(HttpFilterCollection filters)
        {
            filters.Add(new BusinessExceptionAttribute());
            filters.Add(new TestBankAuthorizeAttribute());
        }
    }
}