﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http.SelfHost;
using System.Web.Http.Dispatcher;
using System.Web.Http;
using System.Reflection;
using TestBank.Business.Auth;

namespace TestBank.SelfHost
{
    public class Program
    {
        public static void Main()
        {
            var config = new HttpSelfHostConfiguration("http://localhost:8081");
            //config.Services.Replace(typeof(IAssembliesResolver), new CustomAssemblyResolver());
            config.MessageHandlers.Add(new AuthenticationHandler());
            //ConfigureApi(GlobalConfiguration.Configuration);
            //Database.SetInitializer(new BookInitializer());

            config.Routes.MapHttpRoute(
                "APIDefault",
                "api/{controller}/{id}",
                new { id = RouteParameter.Optional });

            using (var server = new HttpSelfHostServer(config))
            {
                server.OpenAsync();
                Console.WriteLine("Server started at {0}", config.BaseAddress);
                Console.ReadLine();
            }
        }

        public class CustomAssemblyResolver : IAssembliesResolver
        {
            public ICollection<Assembly> GetAssemblies()
            {
                List<Assembly> baseAssemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();
                var controllersAssembly = Assembly.LoadFrom(AppDomain.CurrentDomain.BaseDirectory + "TestBank.Business.dll");
                baseAssemblies.Add(controllersAssembly);
                return baseAssemblies;
            }
        }
    }
}
