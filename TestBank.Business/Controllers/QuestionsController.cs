﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestBank.Entity.Model;
using System.Net.Http;
using System.Web.Http;
using System.Net;
using TestBank.Entity.Utilities;
using TestBank.Entity.Extensions;
using NLog;
using TestBank.Business.Core;
using PagedList;


namespace TestBank.Business.Controllers
{
    public class QuestionsController : ApiController
    {
        Logger logger = LogManager.GetCurrentClassLogger();
        QuestionsManager manager = new QuestionsManager();

        public QuestionsController()
        {
            logger.Debug("Controller constructor.");
        }

        public HttpResponseMessage GetAll()
        {
            HttpResponseMessage response;
            
            // should also be based on the login/authenticated user id
            response = Request.CreateResponse<IEnumerable<Question>>(HttpStatusCode.OK, manager.GetAll());
            return response;
        }

        public HttpResponseMessage GetAll(int pageIndex, int pageSize)
        {
            var response = Request.CreateResponse<PagedModel<Question>>(HttpStatusCode.OK, manager.GetAll(pageIndex, pageSize));
            return response;
        }

        public HttpResponseMessage Post(Question question)
        {
            if (question == null)
            {
                //throw new HttpResponseException(HttpStatusCode.BadRequest);
                var message = string.Format("Request data not in correct format");
                HttpError err = new HttpError(message);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, err);
            }
            question = manager.AddQuestion(question);
            var response = Request.CreateResponse<Question>(HttpStatusCode.Created, question);

            string uri = Url.Link("APIDefault", new { id = question.Id.RemoveRavenIdPrefix() });
            response.Headers.Location = new Uri(uri);
            return response;
        }

        public HttpResponseMessage Put(int id, Question question)
        {
            logger.Debug("Put method of api");
            if (question == null)
            {
                var message = string.Format("Request data not in correct format");
                HttpError err = new HttpError(message);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, err);
            }
            question.Id = id.ToString();
            question = manager.UpdateQuestion(question);
            var response = Request.CreateResponse<Question>(HttpStatusCode.OK, question);

            return response;
        }

        public Question Get(int id)
        {
            Question question = GetWithAnswer(id);

            if (question != null)
            {
                if (question.Options != null && question.Options.Count > 0)
                {
                    foreach (var item in question.Options)
                    {
                        item.SetAnswerSerialize(true);
                    }
                }
                return question;
            }
            else
            {
                var message = string.Format("Question with id = '{0}' not found", id);
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, message));
            }
        }
        
        /// <summary>
        /// Delete not allowed, alternatively in activate the question
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private HttpResponseMessage Delete(int id)
        {
            var question = manager.DeleteQuestion(id);
            if (question != null)
            {
                return Request.CreateResponse<Question>(HttpStatusCode.OK, question);
            }
            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, string.Format("Question not found with id = {0}", id)));
        }

        private Question GetWithAnswer(int id)
        {
            return manager.GetQuestion(id);
        }
    }
}
