﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;
using NLog;
using TestBank.Business.Core;
using TestBank.Entity.Model;
using System.Net.Http;
using System.Net;
using TestBank.Entity.Utilities;
using TestBank.Entity.Extensions;

namespace TestBank.Business.Controllers
{
    public class AssessmentsController : ApiController
    {
        Logger logger = LogManager.GetCurrentClassLogger();
        AssessmentsManager manager = new AssessmentsManager();

        public AssessmentsController()
        {
            logger.Debug("Assessment Controller ctor.");    
        }

        public IEnumerable<Assessment> GetAll()
        {
            return manager.GetAll();
        }

        [HttpPost]
        public HttpResponseMessage Post(Assessment assessment)
        {
            if (assessment == null)
            {
                var message = string.Format("Request data not in correct format");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
            assessment = manager.Post(assessment);
            var response = Request.CreateResponse<Assessment>(HttpStatusCode.Created, assessment);

            string uri = Url.Link("APIDefault", new { id = assessment.Id.ConvertToInt32() });
            response.Headers.Location = new Uri(uri);
            return response;
        }

        public HttpResponseMessage Put(int id, Assessment assessment)
        {
            logger.Debug("Put method of api");
            if (assessment == null)
            {
                var message = string.Format("Request data not in correct format");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
            assessment.Id = id.ToString();
            assessment = manager.UpdateAssessment(assessment);
            var response = Request.CreateResponse<Assessment>(HttpStatusCode.OK, assessment);

            return response;
        }

        public Assessment Get(int id)
        {
            Assessment assessment = manager.GetAssessment(id);

            if (assessment != null)
            {
                return assessment;
            }
            else
            {
                var message = string.Format("Assessment with id = '{0}' not found", id);
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, message));
            }
        }

        public Assessment Get(int id, int pageIndex, int pageSize)
        {
            Assessment assessment = manager.Get(id, pageIndex, pageSize);

            if (assessment != null)
            {
                return assessment;
            }
            else
            {
                var message = string.Format("Assessment with id = '{0}' not found", id);
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, message));
            }
        }

        /// <summary>
        /// Delete not allowed.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private HttpResponseMessage Delete(int id)
        {
            var assessment = manager.Delete(id);
            if (assessment != null)
            {
                return Request.CreateResponse<Assessment>(HttpStatusCode.OK, assessment);
            }
            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, string.Format("Assessment not found with id = {0}", id)));
        }
    }
}
