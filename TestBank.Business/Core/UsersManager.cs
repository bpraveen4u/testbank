﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestBank.Repository;
using TestBank.Entity.Model;
using TestBank.Business.Core.Validators;
using TestBank.Business.Exceptions;
using System.Linq.Expressions;
using TestBank.Entity.Utilities;
using TestBank.Entity.Models;
using TestBank.Entity.Sys;
using TestBank.Entity.Extensions;

namespace TestBank.Business.Core
{
    public class UsersManager
    {
        private IRepository _repository;

        public UsersManager() 
            : this(new TestBankRepository())
        {   
        }

        public UsersManager(IRepository repository)
        {
            _repository = repository;
        }

        #region User Methods

        public IEnumerable<User> GetAll()
        {
            var users = _repository.All<User>();
            var userList = users.Select(m => { return GetUser(m); }).ToList();
            return userList;
        }

        //public User Get(int id)
        //{
        //    return GetUser(id.ToString());
        //}

        public User Get(string userId)
        {
            //var user = Get(x => x.Id == userId.ToLower());
            //if (user != null)
            //{
                return GetUser(userId);
            //}
            //return null;
        }

        //private User Get(Func<User, bool> filter)
        //{
        //    var user = _repository.SingleOrDefault<User>(filter);
        //    return user;
        //}

        public TestBankIdentity ValidateUserLogin(Credentials credentials)
        {
            var user = _repository.Load<User>(RavenIdConverter.Convert(RavenIdPrefix.Users, credentials.User));
            if (user != null)
            {
                var identity = new UserIdentity()
                {
                    ApiKey = Guid.NewGuid(),
                    UserId = user.Id,
                    CreatedDate = DateTime.UtcNow,
                    IsExpired = false,
                    Role = user.Role
                };

                if (user.Role == Roles.Instructor)
                {
                    if (!string.IsNullOrWhiteSpace(credentials.Password))
                    {
                        if (!user.ValidatePassword(credentials.Password))
                        {
                            return null;
                        }
                    }
                }
                else if (user.Role == Roles.Student)
                {
                    //TODO : Get the actual test time insted of 120 or hide the userid
                    if (user.IsLocked)
                    {
                        return null;
                    }
                    else if (DateTime.UtcNow.Subtract(user.CreatedDate.Value).TotalMinutes > 65)
                    {
                        return null;
                    }
                }
                
                AddApiKey(identity);

                var tbIdentity = new TestBankIdentity()
                {
                    UserIdentity = identity
                };

                return tbIdentity;
            }
            return null;
        }

        private void AddApiKey(UserIdentity identity)
        {
            var uniqueId = RavenIdConverter.Convert(RavenIdPrefix.ApiKeyStores, string.Format("{0:yyyyMMdd}", DateTime.UtcNow));
            var apiKeyStore = _repository.Load<ApiKeyStore>(uniqueId);
            if (apiKeyStore == null)
            {
                apiKeyStore = new ApiKeyStore() { Id = uniqueId, CreatedDate = DateTime.UtcNow, LoginUsers = new List<UserIdentity>() };
                apiKeyStore.LoginUsers.Add(identity);
                _repository.Add<ApiKeyStore>(apiKeyStore);
            }
            else
            {
                foreach (var key in apiKeyStore.LoginUsers)
                {
                    if (key.UserId == identity.UserId)
                    {
                        //if (key.CreatedDate.AddMinutes(5) < DateTime.UtcNow)
                        //{
                            key.IsExpired = true;
                        //}
                    }
                }
                apiKeyStore.LoginUsers.Add(identity);
            }
            _repository.Save();
        }

        public User Post(User user)
        {
            UserValidator validator = new UserValidator();
            var results = validator.Validate(user);
            if (results.IsValid)
            {
                //user.UserId = user.UserId.ToLower();
                
                if (Get(user.Id) == null)
                {
                    var userId = RavenIdConverter.Convert(RavenIdPrefix.Users, user.Id.ToLower());
                    user.Id = userId;
                    user.CreatedDate = user.ModifiedDate = DateTime.UtcNow;
                    user.CreatedUser = user.ModifiedUser = userId;
                    if (string.IsNullOrWhiteSpace(user.Password))
                    {
                        user.SetPassword(user.Id.ToLower() + "@111");
                    }
                    else
                    {
                        user.SetPassword(user.Password);
                        user.IsSerializePassword = false;
                        user.Role = Roles.Instructor;
                    }
                    _repository.Add<User>(user);
                    _repository.Save();
                    var newUser = _repository.Load<User>(user.Id);
                    return GetUser(newUser);
                }
                else
                {
                    throw new BusinessException("Invalid Request Format", new List<string>() {"'User Id' already exists please choose another."});
                }
            }
            else
            {
                var errors = results.Errors.Select(e => e.ErrorMessage).ToList();
                throw new BusinessException(errors);
            }
        }

        public User Update(User user)
        {
            var userOriginal = _repository.Load<User>(RavenIdConverter.Convert(RavenIdPrefix.Users, user.Id));
            if (userOriginal != null)
            {
                UserValidator validator = new UserValidator();
                var results = validator.Validate(user);
                if (results.IsValid)
                {
                    if (userOriginal.Id.Equals(user.Id, StringComparison.InvariantCultureIgnoreCase))
                    {
                        userOriginal.Title = user.Title;
                        userOriginal.FirstName = user.FirstName;
                        userOriginal.LastName = user.LastName;
                        userOriginal.PhoneNumber = user.PhoneNumber;
                        userOriginal.ModifiedDate = DateTime.UtcNow;
                        userOriginal.Qualification = user.Qualification;
                        userOriginal.Sort = user.Sort;
                        userOriginal.Email = user.Email;
                        userOriginal.Role = user.Role;
                        _repository.Save();
                    }
                    else
                    {
                        throw new BusinessException("Invalid Request Format", new List<string>() { "'UserId' can not be modified." });
                    }
                }
                else
                {
                    var errors = results.Errors.Select(e => e.ErrorMessage).ToList();
                    throw new BusinessException(errors);
                }
            }

            return GetUser(user.Id);
        }

        /// <summary>
        /// Get User without Prefix
        /// </summary>
        /// <param name="user">User</param>
        /// <returns>Returns User without id prefix</returns>
        private User GetUser(User user)
        {
            user.Id = user.Id.RemoveRavenIdPrefix();
            user.CreatedUser = user.CreatedUser.RemoveRavenIdPrefix();
            user.ModifiedUser = user.ModifiedUser.RemoveRavenIdPrefix();
            return user;
        }

        private User GetUser(string id)
        {
            var user = _repository.Load<User>(RavenIdConverter.Convert(RavenIdPrefix.Users, id));
            if (user == null)
            {
                return null;
            }
            return GetUser(user);
        }

        #endregion
    }
}
