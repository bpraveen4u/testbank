﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestBank.Entity.Model;
using FluentValidation;

namespace TestBank.Business.Core.Validators
{
    public class AssessmentValidator : AbstractValidator<Assessment>
    {
        public AssessmentValidator()
        {
            RuleFor(a => a.TestName).NotEmpty().WithMessage("'TestName' is required.");
            RuleFor(a => a.Duration).InclusiveBetween(5, 60).WithMessage("duration must be in between 5 and 60 minutes");
            RuleFor(a => a.MaxOptions).GreaterThan(1).WithMessage("Maximum Options must be greater than 1.");
            RuleFor(q => q.Questions)
                    .NotEmpty().WithMessage("Atleast 1 question is required for a Test.");
                    //.Must(ValidateQuestionIdExists).WithMessage("Some of the mapped question not exist.")
            RuleFor(q => q.Questions).SetCollectionValidator(new QuestionReferenceValidator());
        }
    }

    public class QuestionReferenceValidator : AbstractValidator<QuestionReference>
    {
        public QuestionReferenceValidator()
        {
            RuleFor(q => q.QuestionId)
                .NotEmpty().WithMessage("'QuestionId' is Required.")
                .Matches(@"[1-9][0-9]*").WithMessage("'QuestionId' must be positive number.")
            ;

        }
    }
}
