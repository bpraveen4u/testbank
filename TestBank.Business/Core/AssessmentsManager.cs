﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestBank.Repository;
using TestBank.Entity.Model;
using TestBank.Entity.Utilities;
using TestBank.Business.Exceptions;
using TestBank.Business.Core.Validators;
using TestBank.Entity.Sys;
using TestBank.Entity.Extensions;

namespace TestBank.Business.Core
{
    public class AssessmentsManager
    {
        private IRepository _repository; 
        //TestBank manager = new ProductManager(new ProductRepositorySqlServer());
        public AssessmentsManager()
        {
            //by default uses the raven database
            _repository = new TestBankRepository();
        }

        public AssessmentsManager(IRepository repository)
        {
            _repository = repository;
        }

        #region Assessment Methods

        public IEnumerable<Assessment> GetAll()
        {
            var userId = GetLoggedUser();
            
            var assessments = _repository.RavenSession.Query<Assessment>().Where(q => q.CreatedUser == userId).ToList();
            var assessmentList = assessments.Select(m =>
            {
                m.Id = m.Id.RemoveRavenIdPrefix();
                m.CreatedUser = m.CreatedUser.RemoveRavenIdPrefix();
                m.ModifiedUser = m.CreatedUser.RemoveRavenIdPrefix();
                return m;
            }).ToList();
            
            return assessmentList;
        }

        /// <summary>
        /// Gets Assessment
        /// </summary>
        /// <param name="id">id of the assessment</param>
        /// <returns>return the Assessment</returns>
        public Assessment GetAssessment(int id)
        {
            return GetAssessment(id.ToString());
        }

        /// <summary>
        /// Get Assessment with the paged questions.
        /// </summary>
        /// <param name="id">assessment id</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>return the Assessment with paged questions</returns>
        public Assessment Get(int id, int pageIndex, int pageSize)
        {
            if (pageIndex < 1)
            {
                throw new BusinessException("'pageIndex' must be greater than 0");
            }

            if (pageSize < 1)
            {
                throw new BusinessException("'pageSize' must be greater than 0");
            }
            var session = _repository.RavenSession;

            var assessment = session.Include<Assessment>(a => a.Questions.Select(q => q.QuestionId)).Load(RavenIdConverter.Convert(RavenIdPrefix.Assessments, id));
            assessment = GetAssessment(assessment); //remove id prefix
            assessment.QuestionDetails = new List<Question>();
            var pagedQuestionRefs = assessment.Questions
                                        .OrderBy(m => m.Sort)
                                        .Skip((pageIndex - 1) * pageSize)
                                        .Take(pageSize)
                                        .ToList();
            foreach (var item in pagedQuestionRefs)
            {
                var question = session.Load<Question>(RavenIdConverter.Convert(RavenIdPrefix.Questions, item.QuestionId));
                if (question != null)
                {
                    question.Id = question.Id.RemoveRavenIdPrefix();
                    assessment.QuestionDetails.Add(question);
                }
            }
            assessment.IsSerializeQuestionDetails = true;

            return assessment;
        }

        /// <summary>
        /// add the new assessment
        /// </summary>
        /// <param name="assessment"></param>
        /// <returns></returns>
        public Assessment Post(Assessment assessment)
        {
            AssessmentValidator validator = new AssessmentValidator();
            var results = validator.Validate(assessment);
            if (results.IsValid)
            {
                //TO-DO validate the questionId's and are also from the login user
                assessment.Id = null;
                assessment.CreatedUser = assessment.ModifiedUser = GetLoggedUser();
                assessment.CreatedDate = assessment.ModifiedDate = DateTime.UtcNow;
                assessment.IsSerializeQuestionDetails = false;
                if (assessment.Questions == null)
                {
                    assessment.Questions = new List<QuestionReference>();
                }
                else
                {
                    assessment.Questions = assessment.Questions.Select(x =>
                    {
                        var id = x.QuestionId.RemoveRavenIdPrefix();
                        x.QuestionId = RavenIdConverter.Convert(RavenIdPrefix.Questions, id);
                        return x;
                    }).ToList();
                }
                _repository.Add<Assessment>(assessment);
                _repository.Save();
                var newAssessment = _repository.Load<Assessment>(assessment.Id);
                return GetAssessment(newAssessment);
            }
            else
            {
                var errors = results.Errors.Select(e => e.ErrorMessage).ToList();
                throw new BusinessException(errors);
            }
        }

        public Assessment UpdateAssessment(Assessment assessment)
        {
            var assessmentOriginal = _repository.Load<Assessment>(RavenIdConverter.Convert(RavenIdPrefix.Assessments, assessment.Id));
            if (assessmentOriginal != null)
            {
                AssessmentValidator validator = new AssessmentValidator();
                var results = validator.Validate(assessment);
                if (results.IsValid)
                {
                    assessmentOriginal.IsSerializeQuestionDetails = false;
                    assessmentOriginal.TestName = assessment.TestName;
                    assessmentOriginal.Sort = assessment.Sort;
                    assessmentOriginal.MaxOptions = assessment.MaxOptions;
                    assessmentOriginal.Link = assessment.Link;
                    assessmentOriginal.ShortLink = assessment.ShortLink;
                    assessmentOriginal.ModifiedDate = DateTime.UtcNow;
                    assessmentOriginal.ModifiedUser = GetLoggedUser();
                    assessmentOriginal.Status = assessment.Status;
                    if (assessment.Questions == null)
                    {
                        assessmentOriginal.Questions = new List<QuestionReference>();
                    }
                    else
                    {
                        assessmentOriginal.Questions = assessment.Questions.Select(x => 
                                                                                       { var id = x.QuestionId.RemoveRavenIdPrefix(); 
                                                                                           x.QuestionId = RavenIdConverter.Convert(RavenIdPrefix.Questions, id); 
                                                                                           return x; 
                                                                                       }).ToList();
                    }
                    assessmentOriginal.Duration = assessment.Duration;
                    assessmentOriginal.Enable = assessment.Enable;
                    _repository.Save();
                }
                else
                {
                    var errors = results.Errors.Select(e => e.ErrorMessage).ToList();
                    throw new BusinessException(errors);
                }
            }

            return GetAssessment(assessment.Id);
        }

        public Assessment Get(string id)
        {
            var assessment = _repository.SingleOrDefault<Assessment>(q => q.Id == id);
            //_repository.IncludeWithLoad<Assessment>(x => x.Questions.Select(q => q.QuestionId), id);
            //_repository.Include<Assessment>(x => x.Questions.Select(q => q.QuestionId));
            //assessment.QuestionDetails = 
            return assessment;
        }

        public Assessment Delete(int id)
        {
            var assessment = GetAssessment(id);
            if (assessment != null)
            {
                _repository.Delete<Assessment>(assessment);
                _repository.Save();
                return assessment;
            }

            return null;
        }

        /// <summary>
        /// Get Assessment without Prefix
        /// </summary>
        /// <param name="assessment">Assessment</param>
        /// <returns>Returns Assessment without id prefix</returns>
        private Assessment GetAssessment(Assessment assessment)
        {
            assessment.Id = assessment.Id.RemoveRavenIdPrefix();
            assessment.CreatedUser = assessment.CreatedUser.RemoveRavenIdPrefix();
            assessment.ModifiedUser = assessment.ModifiedUser.RemoveRavenIdPrefix();
            if (assessment.Questions != null && assessment.Questions.Count > 0)
            {
                assessment.Questions = assessment.Questions.Select(m => { m.QuestionId = m.QuestionId.RemoveRavenIdPrefix(); return m; }).ToList();
            }
            
            return assessment;
        }

        private Assessment GetAssessment(string id)
        {
            var assessment = _repository.Load<Assessment>(RavenIdConverter.Convert(RavenIdPrefix.Assessments, id));
            if (assessment == null)
            {
                return null;
            }
            return GetAssessment(assessment);
        }

        private string GetLoggedUser()
        {
            return TestBankIdentity.GetContextIdentity().UserIdentity.UserId;
        }
        
        #endregion
    }
}
