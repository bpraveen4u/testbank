﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestBank.Repository;
using TestBank.Entity.Model;
using PagedList;
using TestBank.Entity.Utilities;
using TestBank.Business.Exceptions;
using TestBank.Business.Core.Validators;
using TestBank.Entity.Sys;
using TestBank.Entity.Extensions;

namespace TestBank.Business.Core
{
    public class QuestionsManager
    {
        private IRepository _repository; 
        //TestBank manager = new ProductManager(new ProductRepositorySqlServer());
        public QuestionsManager()
        {
            //by default uses the raven database
            _repository = new TestBankRepository();
        }

        public QuestionsManager(IRepository repository)
        {
            _repository = repository;
        }

        #region Questions Methods

        public IEnumerable<Question> GetAll()
        {
            var userId = GetLoggedUser();
            //var questions = _repository.All<Question>();
            var questions = _repository.RavenSession.Query<Question>().Where(q => q.CreatedUser == userId).ToList();
            var questionList = questions.Select(m => { m.Id = m.Id.RemoveRavenIdPrefix(); 
                                                        m.CreatedUser = m.CreatedUser.RemoveRavenIdPrefix();
                                                        m.ModifiedUser = m.CreatedUser.RemoveRavenIdPrefix();
                                                        return m; 
                                                     }).ToList();
            return questionList;
        }

        /// <summary>
        /// Gets Question
        /// </summary>
        /// <param name="id">id of the question</param>
        /// <returns>return the Question</returns>
        public Question GetQuestion(int id)
        {
            return GetQuestion(id.ToString());
        }
        
        public int Count(string userFilter = "")
        {
            if (string.IsNullOrEmpty(userFilter))
            {
                return _repository.Count<Question>();
            }
            else
            {
                return _repository.Count<Question>(m => m.CreatedUser, userFilter);
            }
        }

        public PagedModel<Question> GetAll(int pageIndex, int pageSize)
        {
            if (pageIndex < 1)
            {
                throw new BusinessException("'pageIndex' must be greater than 0");
            }

            if (pageSize < 1)
            {
                throw new BusinessException("'pageSize' must be greater than 0");
            }
            
            int totalResults;
            Raven.Client.RavenQueryStatistics stats;
            var userId = GetLoggedUser();
            //var pagedQuestions = _repository.All<Question>(pageIndex, pageSize, (m => m.Sort), out totalResults);
            
            var pagedItems = _repository.RavenSession.Query<Question>()
                .Statistics(out stats)
                .Where(m => m.CreatedUser == userId)
                .OrderBy(m => m.Id)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToList()
                ;
            totalResults = stats.TotalResults;

            var pagedQuestions = pagedItems.ToList();
            var pageList = new PagedModel<Question> { 
                CurrenctPage = pageIndex, 
                TotalRecords = totalResults, 
                PageSize = pageSize,
                PagedData = pagedQuestions.Select(m => { m.Id = m.Id.RemoveRavenIdPrefix(); return m; }).ToList()
            };
            return pageList;
        }

        /// <summary>
        /// Add question and return the Added question with the id
        /// </summary>
        /// <param name="question">Question to add</param>
        /// <returns>Question</returns>
        public Question AddQuestion(Question question)
        {
            QuestionValidator validator = new QuestionValidator();
            var results = validator.Validate(question);
            if (results.IsValid)
            {
                question.Id = null;
                question.CreatedDate = question.ModifiedDate = DateTime.UtcNow;
                question.CreatedUser = question.ModifiedUser = GetLoggedUser();
                _repository.Add<Question>(question);
                _repository.Save();
                var newQuestion = _repository.Load<Question>(question.Id);
                return GetQuestion(newQuestion);
            }
            else
            {
                var errors = results.Errors.Select(e => e.ErrorMessage).ToList();
                throw new BusinessException(errors);
            }
        }

        public Question UpdateQuestion(Question question)
        {
            var questionOrginal = _repository.Load<Question>(RavenIdConverter.Convert(RavenIdPrefix.Questions, question.Id));
            if (questionOrginal != null)
            {
                QuestionValidator validator = new QuestionValidator();
                var results = validator.Validate(question);
                if (results.IsValid)
                {
                    questionOrginal.Description = question.Description;
                    questionOrginal.InstructorRemarks = question.InstructorRemarks;
                    questionOrginal.Category = question.Category;
                    questionOrginal.Sort = question.Sort;
                    questionOrginal.Weightage = question.Weightage;
                    questionOrginal.CorrectScore = question.CorrectScore;
                    questionOrginal.WrongScore = question.WrongScore;
                    questionOrginal.Options = question.Options;
                    questionOrginal.ModifiedDate = DateTime.UtcNow;
                    questionOrginal.ModifiedUser = GetLoggedUser(); ;
                    _repository.Save();
                }
                else
                {
                    var errors = results.Errors.Select(e => e.ErrorMessage).ToList();
                    throw new BusinessException(errors);
                }
            }

            return GetQuestion(question.Id);
        }

        public Question DeleteQuestion(int id)
        {
            var question = GetQuestion(id);
            if (question != null)
            {
                _repository.Delete<Question>(question);
                _repository.Save();
                return question;
            }

            return null;
        }

        /// <summary>
        /// Get Question without Prefix
        /// </summary>
        /// <param name="question">Question</param>
        /// <returns>Returns Question without id prefix</returns>
        private Question GetQuestion(Question question)
        {
            question.Id = question.Id.RemoveRavenIdPrefix();
            question.CreatedUser = question.CreatedUser.RemoveRavenIdPrefix();
            question.ModifiedUser = question.ModifiedUser.RemoveRavenIdPrefix();
            return question;
        }

        private Question GetQuestion(string id)
        {
            var question = _repository.Load<Question>(RavenIdConverter.Convert(RavenIdPrefix.Questions, id));
            
            if (question != null && question.CreatedUser == GetLoggedUser())
            {
                return GetQuestion(question);
            }
            return null;
        }

        private string GetLoggedUser()
        {
            return TestBankIdentity.GetContextIdentity().UserIdentity.UserId;
        }
        
        #endregion
    }
}
