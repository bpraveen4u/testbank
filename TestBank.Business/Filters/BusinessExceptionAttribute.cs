﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http.Filters;
using TestBank.Business.Exceptions;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestBank.Business.Infrastructure.Logging;
using System.Net.Http.Formatting;

namespace TestBank.Business.Filters
{
    public class BusinessExceptionAttribute : ExceptionFilterAttribute
    {
        private ILogger _logger;
        public BusinessExceptionAttribute()
        {
            _logger = LogFactory.Logger;
        }

        public BusinessExceptionAttribute(ILogger logger)
        {
            _logger = logger;
        }

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {

            //log exception
            //base.OnException(actionExecutedContext);
            var controller = actionExecutedContext.ActionContext.ControllerContext.ControllerDescriptor.ControllerName;
            var action = actionExecutedContext.ActionContext.ActionDescriptor.ActionName;

            var badResponse = new HttpResponseMessage();

            if (actionExecutedContext.Exception is NotImplementedException)
            {
                _logger.Error(actionExecutedContext.Exception);
                badResponse = new HttpResponseMessage(HttpStatusCode.NotImplemented);
                badResponse.Content = new StringContent("Request method is Not implemented.");
            }
            else if (actionExecutedContext.Exception is HttpResponseException)
            {
                throw actionExecutedContext.Exception;
            }
            else if (actionExecutedContext.Exception is BusinessException)
            {
                //throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                //{
                    
                //    Content = new StringContent(context.Exception.Message),
                //    ReasonPhrase = "Exception"
                //});
                var businessException = actionExecutedContext.Exception as BusinessException;
                _logger.Error(businessException);

                badResponse.StatusCode = HttpStatusCode.PreconditionFailed;

                if (businessException.Errors != null)
                {
                    //throw new HttpResponseException(actionExecutedContext.Request.CreateResponse<IEnumerable<string>>(HttpStatusCode.BadRequest, businessException.Errors));
                    badResponse.Content = new ObjectContent(typeof(IEnumerable<string>), businessException.Errors, new JsonMediaTypeFormatter());
                }
                else
                {
                    //throw new HttpResponseException(actionExecutedContext.Request.CreateResponse<string>(HttpStatusCode.BadRequest, businessException.Message));
                    badResponse.Content = new ObjectContent(typeof(IEnumerable<string>), new string[] { businessException.Message }, new JsonMediaTypeFormatter());
                }
            }
            else if (actionExecutedContext.Exception is HttpResponseException)
            {
                _logger.Error((HttpResponseException)actionExecutedContext.Exception);
            }
            else
            {
                _logger.Fatal(actionExecutedContext.Exception);
                badResponse.Content = new StringContent("Server error occured. Please contact administrator");
                badResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            }
            actionExecutedContext.Response = badResponse;
            
            //throw new HttpResponseException(context.Request.CreateResponse<Exception>(HttpStatusCode.BadRequest, context.Exception));
            //throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            //{
            //    Content = new StringContent("An error occurred, please try again or contact the administrator."),
            //    ReasonPhrase = "Critical Exception"
            //});
        }
    }
}
