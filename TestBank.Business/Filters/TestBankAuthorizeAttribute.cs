﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;
using TestBank.Entity.Sys;
using System.Net.Http;
using System.Net;

namespace TestBank.Business.Filters
{
    public class TestBankAuthorizeAttribute : AuthorizeAttribute
    {
        //private readonly AuthManager manager;
        //public TestBankAuthorize(AuthManager manager)
        //{
        //    this.manager = manager;
        //}

        private const string APIKEY = "APIKey";
        private string controllerName;
        private string actionName;

        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            if (actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Count > 0 || actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Count > 0)
            {
                return;
            }

            var headers = actionContext.Request.Headers;
            IEnumerable<string> apiKeys;
            headers.TryGetValues(APIKEY, out apiKeys);
            if (apiKeys == null)
            {
                APIKeyRequired();
            }
            else
            {
                controllerName = actionContext.ControllerContext.ControllerDescriptor.ControllerName;
                actionName = actionContext.ActionDescriptor.ActionName;
                ValidateAPIKey(apiKeys.FirstOrDefault(), actionContext);
                //Validate Acl's
                //ValidateApiAcl();
                return;
            }
        }

        private void ValidateAPIKey(string APIkey, System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            TestBankPrincipal pricipal = null;
            //Check in the cache and get the principal

            TestBank.Business.Infrastructure.Cache.CacheStore.Cache.TryGetValue(APIkey.Replace("\"", ""), out pricipal);
            if (null == pricipal)
            {
                InvalidAPIKey();
            }
            else
            {
                (pricipal.Identity as TestBankIdentity).UserIP = "";
                (pricipal.Identity as TestBankIdentity).UserAgent = actionContext.Request.Headers.UserAgent.ToString();
                (pricipal.Identity as TestBankIdentity).RequestedParams = string.Empty;
                if (actionContext.Request.Method == HttpMethod.Get)
                {
                    //(pricipal.Identity as TestBankIdentity).RequestedParams = Newtonsoft.Json.JsonConvert.SerializeObject(actionContext.Request.Properties);
                }

                (pricipal.Identity as TestBankIdentity).Controller = controllerName;
                (pricipal.Identity as TestBankIdentity).Action = actionName;
                System.Threading.Thread.CurrentPrincipal = pricipal;
            }
        }

        private void InvalidAPIKey()
        {
            var badResponse = new HttpResponseMessage();
            badResponse.StatusCode = HttpStatusCode.Unauthorized;
            badResponse.Content = new StringContent("Invalid API Key");
            throw new HttpResponseException(badResponse);
        }

        private void APIKeyRequired()
        {
            var badResponse = new HttpResponseMessage();
            badResponse.StatusCode = HttpStatusCode.Unauthorized;
            badResponse.Content = new StringContent("API Key Required.");
            throw new HttpResponseException(badResponse);
        }
    }
}
