﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http.Filters;
using System.Threading;
using System.Net;
using System.Web.Http;
using System.Net.Http;

namespace TestBank.Business.Filters
{
    public class BusinessActionAttibute : ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            if (!Thread.CurrentPrincipal.Identity.IsAuthenticated)
            {
                //var identity = new HostIdentity();
                //identity.Controller = actionContext.ControllerContext.ControllerDescriptor.ControllerName;
                //identity.Action = actionContext.ActionDescriptor.ActionName;
                //identity.UserIP = "";
                //identity.UserAgent = actionContext.Request.Headers.UserAgent.ToString();
                ////identity.RequestedParams = Newtonsoft.Json.JsonConvert.SerializeObject(actionContext.Request.Properties);
                //var principal = new HostPrincipal(identity, null);
                //Thread.CurrentPrincipal = principal;
            }
            //validate model state
            ValidateModelState(actionContext);
        }

        private void ValidateModelState(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var errors = new List<string>();

            if (actionContext.ModelState.IsValid)
            {
                validateForNulls(actionContext.ActionArguments, ref errors);
                if (errors.Count <= 0)
                {
                    return;
                }
            }
            else
            {
                foreach (var key in actionContext.ModelState.Keys.Where(key =>
                    actionContext.ModelState[key].Errors.Any()))
                {
                    errors.AddRange(actionContext.ModelState[key].Errors.Select(er => er.ErrorMessage));
                }
            }

            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, errors);
            actionContext.Response.ReasonPhrase = "Invalid parameters /arguments.";
            return;
        }

        private void validateForNulls(Dictionary<string, object> actionArgments, ref List<string> errors)
        {
            foreach (var argument in actionArgments)
            {
                if (argument.Value == null)
                {
                    errors.Add("A value is required but was not present in the request.");
                }
            }
        }
    }
}
