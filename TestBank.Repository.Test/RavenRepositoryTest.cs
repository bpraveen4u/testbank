﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using TestBank.Entity.Model;
using Raven.Client.Document;
using Raven.Client.Linq;
using Raven.Abstractions.Data;
using Raven.Json.Linq;

namespace TestBank.Repository.Test
{
    [TestFixture]
    public class RavenRepositoryTest
    {
        IRepository _repository;// = new TestBankRepository();
        [SetUp]
        public void TestSetup()
        {
            var documentStore = new Raven.Client.Document.DocumentStore { Url = "http://localhost:8080" };
            documentStore.Initialize();
            //var generator = new MultiTypeHiLoKeyGenerator(documentStore, 10);
            //documentStore.Conventions.DocumentKeyGenerator =
            //    entity => generator.GenerateDocumentKey(documentStore.Conventions, entity);
            //documentStore.Conventions.DocumentKeyGenerator = (entity) => documentStore.Conventions.GetTypeTagName(entity.GetType()) + "/";
            _repository = new TestBankRepository(documentStore);
            System.Diagnostics.Debugger.Launch();
        }

        [Test]
        public void AddQuestion()
        {
            Question q = new Question() {Description = "Test desc1", Category="Analytical", CorrectScore = 2, Weightage = 2 };
            q.Options = new List<Option>(){
                new Option() { Id = "A", IsCorrect = true, Text = "Opt1", Type = OptionType.RadioButton},
                new Option() { Id = "B", IsCorrect = false, Text = "Opt2", Type = OptionType.RadioButton},
                new Option() { Id = "C", IsCorrect = false, Text = "Opt3", Type = OptionType.RadioButton},
                new Option() { Id = "D", IsCorrect = false, Text = "Opt4", Type = OptionType.RadioButton}
            };
            
            _repository.Add<Question>(q);
            _repository.Save();
            Assert.AreEqual(1, 1);
        }

        [Test]
        public void UpdateQuestion()
        {
            var q = _repository.SingleOrDefault<Question>(x => x.Id == "Questions/5");
            q.Options[0].IsCorrect = true;
            q.Options[1].IsCorrect = false;
            q.Options[2].IsCorrect = false;
            q.Options[3].IsCorrect = false;
            _repository.Save();
        }

        [Test]
        public void GetQuestion()
        {
            var q = _repository.SingleOrDefault<Question>(x => x.Id == "Questions/2");
            
            Console.WriteLine(q.Options[0].Id);
        }

        [Test]
        public void AddAssessment()
        {
            Assessment test = new Assessment()
            {
                Questions = new List<QuestionReference>()
                { 
                    new QuestionReference() { QuestionId = "Questions/2", Sort = 1 },
                    new QuestionReference() { QuestionId = "Questions/3", Sort = 2 },
                    new QuestionReference() { QuestionId = "Questions/4", Sort = 3 }
                },
                Link = "dsdfd"
            };
            _repository.Add<Assessment>(test);
            _repository.Save();
        }

        [Test]
        public void GetAnswerCorrect()
        {
            //var q = _repository.SingleOrDefault<Assessment>(x => x.Id == "Assessments/2");
            //var test = _repository.Include<Question>(q => q.Options).Load<Assessment>("Assessments/3");
            //Console.WriteLine(test.Questions[0].Options[0].IsCorrect);
            //Console.WriteLine(q.Questions[1].Options[1].IsCorrect);
        }

        [Test]
        public void AddUser()
        {
            var user = new User
            {
                FirstName = "Praveen",
                Email = "praveen@ha.com"
            }.SetPassword("Praveen");

            _repository.Add<User>(user);
            _repository.Save();
        }

        [Test]
        public void VerifyPasswordCorrect()
        {
            var user = _repository.SingleOrDefault<User>(x => x.Id == "Users/1");

            Assert.AreEqual(true, user.ValidatePassword("praveen"));
        }

        [Test]
        public void VerifyPasswordWrong()
        {
            var user = _repository.SingleOrDefault<User>(x => x.Id == "Users/1");

            Assert.AreEqual(false, user.ValidatePassword("Praveen"));
        }

        [Test]
        public void SaveUserAnswersForAssessment()
        {
            string assessment = "Assessments/5";
            var test= _repository.SingleOrDefault<Assessment>(x => x.Id == assessment);
            UserAnswer userAnswers = new UserAnswer();
            userAnswers.Answers = new List<UserAnswer.Answer>();
            userAnswers.UserId = "Users/1";
            userAnswers.AssessmentId = assessment;
                    
            if (test != null)
            {
                foreach (var item in test.Questions)
                {
                    var question = _repository.SingleOrDefault<Question>(x => x.Id == item.QuestionId);
                    var answer = new UserAnswer.Answer()
                    {
                        Question = new QuestionReference()
                        {
                            QuestionId = item.QuestionId,
                            Sort = item.Sort
                        },
                        UserOptions = new List<Option>(){
                                question.Options[0]
                            }
                    };
                    answer.ValidateAnswer(question.Options);
                    userAnswers.Answers.Add(answer);
                }
                _repository.Add<UserAnswer>(userAnswers);
                _repository.Save();
            }
            else
            {
                Assert.Fail("No Assessment with given id");
            }
            
        }

        [Test]
        public void GetPagedQuestions()
        {
            var dds = _repository.Count<Question>();
            var dds1 = _repository.Count<Question>(m => m.CreatedUser, "Users/1");
        }

        [Test]
        public void TestRavenSessionDirectAccess()
        {
            Raven.Client.RavenQueryStatistics stats;
            var session = _repository.RavenSession;
            //var t = session.Query<Assessment>()
            //    .Customize(x => x.Include<Assessment>(a => a.Questions.Select(q => q.QuestionId)))
            //    .Statistics(out stats)
            //    .Skip(0 * 2)
            //    .Take(2)
            //    ;


            var test = session.Include<Assessment>(a => a.Questions.Select(q => q.QuestionId)).Load("Assessments/10");

            test.QuestionDetails = new List<Question>();
            foreach (var item in test.Questions.Skip(0).Take(2))
            {
                var ss = session.Load<Question>(item.QuestionId);
                test.QuestionDetails.Add(ss);
            }
            
            var kk = test;

            //foreach (var question in test.Questions)
            //{
            //    var test = session.Load<Question>(question.QuestionId, "", "").ToList().Skip;
            //}
        }

        [Test]
        public void BulkRenamePropertyInRavenDb_UserAnswers()
        {
            var store = _repository.Store;
            store.DefaultDatabase = "TestBank";
            store.DatabaseCommands.UpdateByIndex("UpdateCreatedDateRenameUserAnswers",
              new IndexQuery(),
              new [] {new PatchRequest() {
                  Type = PatchCommandType.Rename,
                  Name = "CreationDate",
                  Value = new RavenJValue("CreatedDate"),
              }
            });
        }

        [Test]
        public void BulkRenamePropertyInRavenDb_Questions()
        {
            var store = _repository.Store;
            store.DefaultDatabase = "TestBank";
            store.DatabaseCommands.UpdateByIndex("UpdateCreatedDateRenameQuestions",
              new IndexQuery(),
              new[] {new PatchRequest() {
                  Type = PatchCommandType.Rename,
                  Name = "CreationDate",
                  Value = new RavenJValue("CreatedDate"),
              }
            });
        }

        [Test]
        public void BulkRenamePropertyInRavenDb_Assessments()
        {
            var store = _repository.Store;
            store.DefaultDatabase = "TestBank";
            store.DatabaseCommands.UpdateByIndex("UpdateCreatedDateRenameAssessments",
              new IndexQuery(),
              new[] {new PatchRequest() {
                  Type = PatchCommandType.Rename,
                  Name = "CreationDate",
                  Value = new RavenJValue("CreatedDate"),
              }
            });
        }
    }
}
