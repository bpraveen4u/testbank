﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TestBank.Entity.Model;
using TestBank.Web.ViewModels;
using TestBank.Entity.Utilities;
using TestBank.Entity.Extensions;

namespace TestBank.Web.Infrastructure.AutoMapper.Profiles
{
    public class AssessmentViewModelMapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Assessment, AssessmentViewModel>()
                .ForMember(x => x.Id, o => o.MapFrom(m => m.Id.ConvertToInt32()))
                .ForMember(x => x.TestName, o => o.MapFrom(m => m.TestName))
                .ForMember(x => x.MaxOptions, o => o.MapFrom(m => m.MaxOptions))
                .ForMember(x => x.CreatedDate, o => o.MapFrom(m => m.CreatedDate))
                .ForMember(x => x.CreatedUser, o => o.MapFrom(m => m.CreatedUser))
                .ForMember(x => x.ModifiedDate, o => o.MapFrom(m => m.ModifiedDate))
                .ForMember(x => x.ModifiedUser, o => o.MapFrom(m => m.ModifiedUser))
                .ForMember(x => x.Enable, o => o.MapFrom(m => m.Enable))
                .ForMember(x => x.Questions, o => o.MapFrom(m => m.Questions))
                .ForMember(x => x.ShortLink, o => o.MapFrom(m => m.ShortLink))
                .ForMember(x => x.Link, o => o.MapFrom(m => m.Link))
                ;
            ;

            Mapper.CreateMap<QuestionReference, QuestionReferenceViewModel>()
                .ForMember(x => x.QuestionId, o => o.MapFrom(m => m.QuestionId.RemoveRavenIdPrefix()))
                .ForMember(x => x.Sort, o => o.MapFrom(m => m.Sort))
                ;
        }
    }

    public class AssessmentMapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<AssessmentViewModel, Assessment>()
                .ForMember(x => x.Id, o => o.Ignore())
                .ForMember(x => x.TestName, o => o.MapFrom(m => m.TestName))
                .ForMember(x => x.MaxOptions, o => o.MapFrom(m => m.MaxOptions))
                .ForMember(x => x.CreatedDate, o => o.MapFrom(m => m.CreatedDate))
                .ForMember(x => x.CreatedUser, o => o.MapFrom(m => m.CreatedUser))
                .ForMember(x => x.ModifiedDate, o => o.MapFrom(m => m.ModifiedDate))
                .ForMember(x => x.ModifiedUser, o => o.MapFrom(m => m.ModifiedUser))
                .ForMember(x => x.Enable, o => o.MapFrom(m => m.Enable))
                .ForMember(x => x.Questions, o => o.MapFrom(m => m.Questions))
                .ForMember(x => x.ShortLink, o => o.MapFrom(m => m.ShortLink))
                .ForMember(x => x.Link, o => o.MapFrom(m => m.Link));
            ;

            Mapper.CreateMap<QuestionReferenceViewModel, QuestionReference>()
                .ForMember(x => x.QuestionId, o => o.MapFrom(m => m.QuestionId))
                .ForMember(x => x.Sort, o => o.MapFrom(m => m.Sort))
                ;
        }
    }
}